#!/usr/bin/env python3
from netius.servers import WSGIServer
from main import Application

app = Application()

server = WSGIServer(app=app.request)
server.serve(
    #host="0.0.0.0",
    port=8080,
)


"""  # WSGIServer
from lib.wsgiserver import WSGIServer
from main import application

website = WSGIServer(application, host="0.0.0.0", port=4433, numthreads=4, server_name=" ", certfile="ssl/fullchain.pem", keyfile="ssl/privkey.pem")
website.start()
"""

"""  # Waitress
from waitress import serve
from main import application

serve(application, port=8080, ident="")
"""

"""

"""
