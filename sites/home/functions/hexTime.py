import time, calendar
from pymeeus import Sun, Epoch


DAY = 86400  # seconds in a day
EPOCH = 1135123200  # beginning of first day


# Main function
def hexTime(info, param):
    
    if param == "A":
        charset = "0123456789ABCDEF"
    else:
        charset = [f"&#xE00{x};" for x in "0123456789ABCDEF"]
    
    return format_time(time.time(), "{y} : {mm} : {d} . {hh} {ss}", charset), True


def get_hex(n, charset, size):
    x = n%16
    new = n//16
    if new == 0:
        return charset[0]*(size-1) + charset[x]
    return get_hex(new, charset, size-1) + charset[x]


def year_time(t, offset):
    local = time.gmtime(t)
    
    last_sol = Sun.Sun.get_equinox_solstice(local.tm_year, target="winter")
    last_sol = last_sol + 10.5/24  # apply time zone 0 (in days)
    last = last_sol.get_full_date(utc=True)
    
    if (last[1] == local[1] and last[2] > local[2]) or last[1] > local[1]:
        last_sol = Sun.Sun.get_equinox_solstice(local.tm_year-1, target="winter")
        last_sol = last_sol + 10.5/24  # apply time zone 0 (in days)
        last = last_sol.get_full_date(utc=True)
    
    last_c = calendar.timegm((last[0], last[1], last[2], 0, 0, 0)) + offset  # lose time of day. the year starts at the beginning of the day, not the solstice
    local_c = t - offset
    
    year = last[0] - time.gmtime(EPOCH).tm_year
    seconds = (local_c - last_c)
    
    return year, seconds


def time16(t, charset=None, tz=7):
    time_offset = (((tz/16)*24) - 10.5) * 3600  # convert new time zone to current time zone, convert to offset in seconds
    
    year, y_seconds = year_time(t, time_offset)

    y_sec = int ( y_seconds * 65536 / 86400 )  # the second of the year
    d_sec = int(y_sec % 65536)
    second = int(d_sec % 256)  # seconds
    
    hour = d_sec // 256  # hours
    
    y_day = int(y_seconds // DAY)
    day = y_day % 16  # days
    
    month = y_day // 16  # months
    
    return year, month, day, hour, second


def format_time(t, form="{y}:{mm}:{d}.{hh}_{ss}", charset="0123456789ABCDEF", tz=7):
    vals = time16(t, charset, tz)
    
    year = get_hex(vals[0], charset, 1)
    short_month = get_hex(vals[1], charset, 1)
    month = get_hex(vals[1], charset, 2)
    day = get_hex(vals[2], charset, 1)
    short_hour = get_hex(vals[3], charset, 2)
    hour = get_hex(vals[3], charset, 2)
    short_second = get_hex(vals[4], charset, 2)
    second = get_hex(vals[4], charset, 2)
    
    return form.format(y=year, m=short_month, mm=month, d=day, h=short_hour, hh=hour, s=short_second, ss=second)
