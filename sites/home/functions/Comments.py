from urllib import parse
import os
import csv
import time
from html import escape as escape_html


def escape_path(path):
    allowed = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_+().0123456789"
    new = ""
    for ch in path:
        if ch in allowed:
            new += ch
        else:
            new += "-"
    if new[0] == ".":
        new = "-" + new
    return new

def new_message(key, info, path):
    values = {
        "username": info.request_params["username"] if "username" in info.request_params else "",
        "email":    info.request_params["email"]    if "email"    in info.request_params else "",
        "private":  info.request_params["private"]  if "private"  in info.request_params else "",
        "message":  info.request_params["message"]  if "message"  in info.request_params else False,
        
        "reviewed": "N",
        "ip":       info.env['REMOTE_ADDR'],
        "time":     int(time.time()),
    }
    
    if values["message"]:
        with open(path, "a", newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=["ip", "time", "username", "email", "private", "message", "reviewed"], extrasaction="ignore")
            writer.writerow(values)
            
        # This is how I send a notification
        with open(os.path.join(os.path.expanduser("~"), "NEW COMMENT.txt"), "a") as notif:
            notif.write(key+"\n")
        
        return True
    
    return False


def Comments(info, param):
    key = param if param else escape_path(info.request_path)
    comment_path = info.site_path+"comments/"+key+".csv"
    
    submit_message = ""
    if info.env["REQUEST_METHOD"] == "POST":
        submitted = new_message(key, info, comment_path)
        if submitted:
            submit_message = "Message submitted successfully!"
        else:
            submit_message = "Failed to submit message :("
        
    form = f"""
        <form method="POST">
            <div class="left">
                Username: (optional)<br>
                <input type="text" name="username" id="user"><br>
                
                Email: (only if you want a reply, not public)<br>
                <input type="text" name="email" id="em"><br>
                
                <input type="checkbox" name="private" id="private">
                <label for="private">Keep comment private</label><br>
            </div>
            
            <div class="right">
                Message:*<br>
                <textarea name="message" id="msg" rows="3"></textarea><br>
                <input type=submit value="Submit for review">
            </div>
        </form>
    """
    
    message_html = ""
    
    if os.path.exists(comment_path):
        with open(comment_path, "r", newline='') as csvfile:
            reader = csv.DictReader(csvfile, ["ip", "time", "username", "email", "private", "message", "reviewed"], restval="")
            for row in reader:
                if row["ip"] != "ip":
                    
                    if row["private"] != "on" and row["reviewed"] == "Y":
                        username = row["username"]
                        message = escape_html(row["message"])
                        send_time = time.strftime("%Y.%m.%d-%H:%M:%S", time.gmtime(int(row["time"])))
                        message_html = f"""
                        <div class="comment">
                            <i><b>{username}</b> wrote at {send_time}</i><br>
                            <p>{message}</p>
                        </div>
                        """ + message_html
                
    else:  # create file if it does not exist
        with open(comment_path, "a", newline='') as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=["ip", "time", "username", "email", "private", "message", "reviewed"], extrasaction='ignore')
            writer.writeheader()
            
    if message_html == "":
        message_html = "<p><i>No comments yet</i></p>"
    else:
        message_html = "<p>Newest message first</p>" + message_html
    
    html = f"""
    <div class="comments">
        <p><b>{submit_message}</b></p>
        {form}
        {message_html}
    </div>
    """
    return html, False
