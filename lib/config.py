
class Config:
    def __init__(self, path=None):
        self.options = [("main", {})]  # the data
        self.parameters = ["main"]  # the filter
        if path is not None:
            self.parse(open(path).read())
    
    # read config from a file
    def parse(self, data):
        parameter = "main"
        for line in data.split("\n"):
            if len(line) > 0 and line[0] != "#":
                if line[0] == "[" and line[-1] == "]":
                    parameter = line[1:-1]
                    self.options += [(parameter, {})]
                else:
                    parts = line.split("=", 1)
                    if len(parts) == 1:
                        self.options[-1][1][line] = "True"
                    else:
                        self.options[-1][1][parts[0]] = parts[1]
    
    # return the value of the given key, using the current filter
    def __getitem__(self, key):
        if type(key) is tuple:
            value = key[1]
            key = key[0]
        else:
            value = ""
        
        parameters = self.parameters
        for part in self.options:
            requirements = part[0].split(",")
            requirements_satisfied = True
            for requirement in requirements:
                if requirement not in parameters:
                    requirements_satisfied = False
                    break
            if requirements_satisfied:
                if key in part[1]:
                    value = part[1][key]
        return value
    
    # return the value of the given key
    def get(self, key, parameters=None, default=""):
        if parameters is None:
            parameters = self.parameters
        else:
            parameters = ["main"] + parameters
        value = default
        for part in self.options:
            requirements = part[0].split(",")
            requirements_satisfied = True
            for requirement in requirements:
                if requirement not in parameters:
                    requirements_satisfied = False
                    break
            if requirements_satisfied:
                if key in part[1]:
                    value = part[1][key]
        return value
    
    # set the filter
    def set_parameters(self, parameters, add_main=True):
        if add_main:
            parameters = ["main"] + parameters
        self.parameters = parameters
    
    # returns all keys and values under a heading
    def get_category(self, name, parameters=None):  # returns sections [name] and [name,parameter,param...] but not [parameter] or [name,other]
        if parameters is None:
            parameters = [name] + self.parameters
        else:
            parameters = ["main", name] + parameters
        
        category = {}
        for part in self.options:
            requirements = part[0].split(",")
            if name in requirements:
                requirements_satisfied = True
                for requirement in requirements:
                    if requirement not in parameters:
                        requirements_satisfied = False
                        break
                if requirements_satisfied:
                    for key in part[1]:
                        category[key] = part[1][key]
        return category
