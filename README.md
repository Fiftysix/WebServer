# WebServer

## A WSGI server for serving basic websites

Copyright 2020, 2021 Benjie Fleming

This software is licensed under the **GNU Affero General Public License v3.0**.



Examples can be seen at https://fiftysix.scot and https://56.is

Basic use:

-   Populate sites.conf
-   Add a site in `sites/`
-   Add and populate a `sites/[site]/site.conf`
-   Add content in `sites/[site]/files/`
-   Add any new features in `generators/`, `generators/pageserver/modules`, `sites/[site]/functions` or `lib/`
-   Edit `launch.py` to your liking. Then run it. (or find another way to call `main:Generator().request`)