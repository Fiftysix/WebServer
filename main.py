import os
from lib.config import Config
import tldextract
import traceback
import urllib
import httpagentparser


# class to hold information
class RequestInfo:
    def __init__(self, env, site_name, site_path, request_path, content_path, request_params, user_agent, server_config, site_config, response):
        self.env = env
        self.site_name = site_name
        self.site_path = site_path
        self.request_path = request_path
        self.content_path = content_path
        self.request_params = request_params
        self.user_agent = user_agent
        self.server_config = server_config
        self.site_config = site_config
        self.response = response
        
        # Used by generators:
        self.file = None
        self.modules = None


class Application:
    def __init__(self):
        # load server config
        self.server_config = Config("sites.conf")
        
        # load generators
        # TODO: make a generator class
        self.generators = {}
        for generator_name in os.listdir("generators"):
            path = f"generators/{generator_name}/{generator_name}.py"
            if os.path.exists(path):
                # load the generator
                program = compile(open(path, "r").read(), path, "exec")
                names = {"__name__": generator_name}
                exec(program, names)
                
                if "Generator" in names:
                    self.generators[generator_name] = names["Generator"](f"generators/{generator_name}/")
                else:
                    raise Exception("Class \"Generator\" not found in "+path)


    # prints some details of a request
    def log_request(self, info):
        query_string = ("?" + info.env["QUERY_STRING"]) if info.env["QUERY_STRING"] else ""
        print(f"{info.env['REQUEST_METHOD']} {info.env['REMOTE_ADDR']} | {info.site_name}:{info.request_path}{query_string} {info.response['code']}")
    
    
    def generate_config_parameters(self, env):
        # Parse subdomains
        domain = tldextract.extract(env["HTTP_HOST"])
        subdomains = domain.subdomain.split(".")
        tsd = subdomains[-1]  # tsd = top sub domain
        
        # 
        parameters = [
            "host:"+env["HTTP_HOST"],
            "domain:"+domain.registered_domain,
            "tsd:"+tsd,
            #TODO:
            # http/https
        ]
        
        # Add subdomains
        for subdomain in subdomains:
            parameters += ["subdomain:"+subdomain]
            
        # Add HTTP request headers
        for var in env:
            if var[:5] == "HTTP_":
                parameters += ["header:"+var[5:].title().replace("_", "-")]
        
        # Add all sections of the request path
        # (will be simplified when wild-cards are implemented in Config)
        path = ""
        for part in env["PATH_INFO"].split("/"):
            if part != "":
                path += "/"+part
                parameters += ["path:"+path]
        
        return parameters


    def request(self, environment, start_response):
        # wrap everything in a try-except so we can serve a custom error message
        try:
            # ensure required values are present
            env = {
                "HTTP_HOST": "",
                "PATH_INFO": "",
            }
            env.update(environment)
            
            # Block requests that we don't like
            if (len(env["PATH_INFO"]) == 0 or env["PATH_INFO"][0] != "/" or
                    len([x for x in ["//", "..", ":", "{", "}", ",", "$", ";", "\\"] if x in env["PATH_INFO"]]) > 0):
                start_response("400", [])
                return [b"The given path was invalid"]
            
            
            # Generate parameters and apply to config
            parameters = self.generate_config_parameters(env)
            self.server_config.set_parameters(parameters)
            
            # Read the server config
            site = self.server_config["name"]
            
            try:  # TODO: find another way to do this
                path_cut = int(self.server_config["path_cut"])
            except ValueError:
                path_cut = 0
            req_path = env["PATH_INFO"][path_cut:]
            
            # Fix invalid path
            if len(req_path) == 0 or req_path[0] != "/":
                req_path = "/" + req_path
            
            # Load site config  # TODO: Load sites at startup
            if not os.path.exists("sites/"+site+"/site.conf"):
                raise Exception("path sites/"+site+"/site.conf does not exist")
            config = Config("sites/"+site+"/site.conf")
            config.set_parameters(parameters)
            
            # Handle ip whitelist
            whitelist = config.get("whitelist", default=None)
            if whitelist is not None:
                if env["REMOTE_ADDR"] not in whitelist.split(","):
                    start_response("403", [("Content-Type", "text/plain")])
                    return [b""]
            
            # Read request parameters
            request_params = {}
            if env["REQUEST_METHOD"] == "GET":
                request_params = dict(urllib.parse.parse_qsl(env["QUERY_STRING"], True))
            elif env["REQUEST_METHOD"] == "POST":
                request_data = str(env["wsgi.input"].read(), "utf-8")
                request_params = dict(urllib.parse.parse_qsl(request_data, True))
            
            # Parse user agent
            if "HTTP_USER_AGENT" in env:
                user_agent = httpagentparser.detect(env["HTTP_USER_AGENT"], True)
            else:
                user_agent = httpagentparser.detect("", True)
                
                
            # Generate information to be used by generators and their modules
            info = RequestInfo(
                env =              env,
                site_name =        site,
                site_path =        "sites/"+site+"/",
                request_path =     req_path,
                content_path =     "sites/"+site+"/files"+req_path,
                request_params =   request_params,
                user_agent =       user_agent,
                server_config =    self.server_config,
                site_config =      config,
                response =         {
                    "code":            200,
                    "headers":         [],
                    "content":         b""
                                   },
            )
            
            
            # Find name of selected generator
            generator = config["generator"]
            if generator not in self.generators:
                raise Exception("Generator "+generator+" does not exist")
            
            # Execute the generator
            self.generators[generator].request(info)
            
            # Log request
            self.log_request(info)
            
            # Respond
            start_response(str(info.response["code"]), info.response["headers"])
            return [info.response["content"]]
        
        except Exception as e:
            # return custom error message
            print("Error:", environment["REQUEST_METHOD"], environment["REMOTE_ADDR"], environment["PATH_INFO"], environment["QUERY_STRING"])
            traceback.print_exc()
            start_response("500", [("Content-Type", "text/plain")])
            return [b"An internal server error occured"]
