import os

class Generator:
    def __init__(self, path):
        pass
    
    def request(self, info):
        if info.site_config["mode"] == "absolute":
            info.response["headers"] += [("Location", info.site_config["location"])]
        else:
            info.response["headers"] += [("Location", os.path.join(info.env["PATH_INFO"], info.site_config["location"]))]
            
        info.response["code"] = 301
