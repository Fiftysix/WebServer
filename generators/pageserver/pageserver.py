import tldextract
import os
import mimetypes
import time
import traceback
from lib.config import Config


class FileInfo:
    def __init__(self, mimetype, config, content, modules):
        self.mimetype = mimetype
        self.config = config
        self.content = content
        self.modules = modules


class Generator:
    
    def __init__(self, path):
        
        # Load modules
        # TODO: make a module class
        self.modules = {}
        self.mime_modules = {}
        for module_name in os.listdir(path+"modules/"):
            mod_path = f"{path}modules/{module_name}/{module_name}.py"
            if os.path.exists(mod_path):
                # Load module
                program = compile(open(mod_path, "r").read(), mod_path, "exec")
                names = {"__name__": f"{path}modules/{module_name}/"}
                exec(program, names)
                
                if "Module" in names:
                    self.modules[module_name] = {"module": names["Module"]()}
                else:
                    raise Exception("Class \"Module\" not found in "+mod_path)
                
            else:
                raise Exception(mod_path+" was not found.")
            
            # Load module configuration file
            conf_path = f"{path}modules/{module_name}/{module_name}.conf"
            if os.path.exists(conf_path):
                self.modules[module_name]["config"] = Config(conf_path)
            else:
                self.modules[module_name]["config"] = Config()
        
        # Read module configuration files
        for module_name in self.modules:
            
            # Ensure module dependencies are met
            for depend in self.modules[module_name]["config"].get_category("dependencies"):
                if depend not in self.modules:
                    raise Exception(f"Module {module_name} depends on {depend}, which was not found")
            
            # Sort modules by mime-type
            for mimetype in self.modules[module_name]["config"].get_category("mimetypes"):
                if mimetype not in self.mime_modules:
                    self.mime_modules[mimetype] = []
                self.mime_modules[mimetype] += [module_name]


    # called when the server recieves a request for this generator
    def request(self, info):
        if os.path.exists(info.content_path):
            
            # Handle directories
            if os.path.isdir(info.content_path):
                if info.request_path[-1] != "/":
                    info.response["headers"] += [("Location", info.env["PATH_INFO"] + "/")]
                    info.response["code"] = 301
                    
                else:
                    # Find the first file named "index" to serve
                    index = None
                    for item in os.listdir(info.content_path):
                        item_path = info.content_path + item
                        if not os.path.isdir(item_path):
                            if os.path.splitext(item)[0] == "index":
                                index = item_path
                                break
                    
                    if index is not None:
                        info.content_path = index
                        self.handle_file(info)
                    else:
                        # No index page
                        self.file_not_found()
            
            # Handle files
            else:
                self.handle_file(info)
                
        else:
            self.file_not_found(info)
    
    
    def file_not_found(self, info):
        info.response["code"] = 404
        info.response["content"] = b"File not found"
        info.response["headers"] += [("Content-Type", "text/plain")]
        # TODO: try to load a 404 module
        

    def handle_file(self, info):
        content = open(info.content_path, "rb").read()
        
        info.response["code"] = 200
        
        # Read config from top of file
        config_data = b""
        lines = content.split(b"\n", 1)
        while lines[0][0:2] == b"#!":
            config_data += lines[0][2:]+b"\n"
            content = lines[1]
            lines = content.split(b"\n", 1)
        file_config = Config()
        if config_data != "":
            file_config.parse(str(config_data, "utf-8"))
        
        # Get mimetype of file
        if file_config["mimetype"]:
            mime = [file_config["mimetype"], None]
        else:
            mime = mimetypes.guess_type(info.content_path)
        
        # Get list of modules for file
        file_modules = file_config["modules", None]
        if file_modules is None:
            if mime[0] in self.mime_modules:
                file_modules = self.mime_modules[mime[0]]
            else:
                file_modules = []
        else:
            file_modules = file_modules.split(",")
        
        # Sort modules by position specified in config (a replacement for ordering dependencies correctly)
        sorted_file_modules = []
        module_positions = {}
        for name in file_modules:
            if name in self.modules:
                position = int(self.modules[name]["config"].get("position", default="0"))
                if position not in module_positions:
                    module_positions[position] = []
                module_positions[position] += [name]
                
        for pos in sorted(module_positions):  # sorted(dict) returns sorted list of keys
            sorted_file_modules += module_positions[pos]
        file_modules = sorted_file_modules
        
        # Generate information to be used by modules
        info.file = FileInfo(
            mimetype = mime,
            config = file_config,
            content = content,
            modules = file_modules,
        )
        info.modules = self.modules
    
        # Pass file through all selected modules
        for module in file_modules:
            if module.strip() != "":
                try:
                    self.modules[module]["module"].apply(info)
                except Exception as e:
                    print("Error in module "+module+":")
                    traceback.print_exc()
        
        
        # Complete response
        info.response["content"] = info.file.content
        
        info.response["headers"] += [("Content-Type", info.file.mimetype[0] if info.file.mimetype[0] is not None else "text/plain")]
        if info.file.mimetype[1] is not None:
            info.response["headers"] += [("Content-Encoding", info.file.mimetype[1])]
