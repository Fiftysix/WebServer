import os


class Module:
    def apply(self, info):
        templates = info.file.config["templates"].split(",")
        if len(templates) > 1 or len(templates[0]) > 0:
            for template in templates:
                info.file.content = self.apply_template({"content": info.file.content}, template, info)


    def apply_template(self, values, template, info):
        if os.path.exists(info.site_path+"/templates/"+template):
            data = open(info.site_path+"/templates/"+template, "rb").read()
            for name in values:
                val = values[name]
                if type(val) == str:
                    val = bytes(val, "utf-8")
                data = data.replace(bytes("{"+name+"}", "utf-8"), val)
            return data
        else:
            return b"Template not found"
