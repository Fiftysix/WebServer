import os
import markdown as md
import traceback


class Module:
    def apply(self, info):
        self.markdown(info)
        info.modules["html_template"]["module"].apply(info)
        
        info.response["mimetype"] = ["text/html", None]


    def markdown(self, info):
        extensions = ['nl2br', 'pymdownx.tilde', 'pymdownx.caret', 'pymdownx.escapeall', 'tables', 'smarty', 'pymdownx.smartsymbols', 'md_in_html', 'toc']
        
        parts = [""]
        escape = False
        for character in str(info.file.content, "utf-8"):
            if character == "\\" and not escape:
                escape = True
            elif character == "$" and not escape:
                parts += [""]
            else:
                if escape:
                    parts[-1] += "\\"
                    escape = False
                parts[-1] += character
        html = ""
        even = False
        imv_count = 0
        idx = 0
        
        # Detect if the browser supports loading=lazy
        has_lazy_load = False  # check if browser supports lazy loading
        if info.user_agent["platform"]["name"] is not None and info.user_agent["browser"]["name"] is not None:
            try:
                ua_browser_version = int(info.user_agent["browser"]["version"].split(".", 1)[0])
            except (ValueError, AttributeError):
                ua_browser_version = 0
            
            lazy_load_platforms = {  # versions of browsers that have the feature
                "chrome":  76,
                "edge":    79,
                "firefox": 75,
            }
            # not dealing with android browsers for now
            if (info.user_agent["platform"]["name"].lower() != "android" and
                info.user_agent["browser"]["name"].lower() in lazy_load_platforms and
                lazy_load_platforms[info.user_agent["browser"]["name"].lower()] <= ua_browser_version):
                has_lazy_load = True
        
        new_parts = [">"]
        for part in parts:
            converted = False
            if even:
                if len(part) > 7:
                    if part[:5] == "image" and len(part.split(":", 2)) == 3:
                        parameters = part.split(":", 2)
                        label = md.markdown(parameters[1], output_format="html5", extensions=extensions)
                        src = parameters[2]
                        content = f"<img src=\"{src}\" loading=\"lazy\">"
                        new_parts += [open("generators/pageserver/modules/markdown/templates/reveal.html", "r").read().format(idx=idx, text=label, content=content)]
                        converted = True
                        idx += 1
                        
                    elif part[:6] == "imlist":
                        images = part.split("\n")
                        if not has_lazy_load:
                            imlistA = f"""
                            <div class="image_list">
                            """
                            imlistB = f"""
                            </div>
                            """
                            for im_idx in range(1, len(images)):
                                imlistA += f"""
                                <div>
                                    <a href="{images[im_idx]}?size=large" target="_blank"><img src="{images[im_idx]}?size=small" loading="lazy"></a>
                                    <a href="{images[im_idx]}" target="_blank">Source Image</a>
                                </div>
                                """
                            
                            new_parts += [imlistA + imlistB]
                        
                        else:
                            imlistA = f"""
                            <div class="image_viewer">
                                
                            """
                            imlistB = f"""
                                <input type="radio" class="imv_close" id="close{imv_count}" name="imv{imv_count}" checked>
                                <div class="outer">
                                    <label for="close{imv_count}" class="background"></label>
                                    <div class="content">
                            """
                            imlistC = f"""
                                        <label class="close" for="close{imv_count}">&times;</label>
                                    </div>
                                </div>
                            </div>
                            """
                            for im_idx in range(1, len(images)):
                                imlistB += f"""
                                        <input type="radio" id="img{im_idx}:{imv_count}" name="imv{imv_count}">
                                        <label for="img{im_idx-1}:{imv_count}">&#10094;</label>
                                        <label for="img{im_idx+1}:{imv_count}">&#10095;</label>
                                        <a href="{images[im_idx]}" target="_blank">View Source Image</a>
                                        <img src="{images[im_idx]}?size=large" loading="lazy">
                                """
                                imlistA += f"""
                                <label for="img{im_idx}:{imv_count}"><img src="{images[im_idx]}?size=small" loading="lazy"></label>
                                """
                            
                            new_parts += [imlistA + imlistB + imlistC]
                            
                        converted = True
                        imv_count += 1
                        idx += 1
                        
                    elif part[:5] == "popup" and len(part.split(":", 2)) == 3:
                        parameters = part.split(":", 2)
                        label = md.markdown(parameters[1], output_format="html5", extensions=extensions)
                        content = md.markdown(parameters[2], output_format="html5", extensions=extensions)
                        new_parts += [open("generators/pageserver/modules/markdown/templates/popup.html", "r").read().format(idx=idx, text=label, content=content)]
                        converted = True
                        idx += 1
                        
                    elif part[:6] == "reveal" and len(part.split(":", 2)) == 3:
                        parameters = part.split(":", 2)
                        label = md.markdown(parameters[1], output_format="html5", extensions=extensions)
                        content = md.markdown(parameters[2], output_format="html5", extensions=extensions)
                        new_parts += [open("generators/pageserver/modules/markdown/templates/reveal.html", "r").read().format(idx=idx, text=label, content=content)]
                        converted = True
                        idx += 1
                    
                    elif part[:8] == "function" and len(part.split(":", 2)) == 3:
                        _, name, parameters = part.split(":", 2)
                        if os.path.exists(info.site_path+"functions/"+name+".py"):
                            py_file = open(info.site_path+"functions/"+name+".py", "r").read()
                            
                            program = compile(py_file, info.site_path+"functions/"+name+".py", "exec")
                            names = {"__name__": __name__+"functions/"+name+".py"}
                            exec(program, names)
                            
                            if name in names:
                                function = names[name]
                                try:
                                    # Call selected function
                                    text, use_md = function(info, parameters)
                                    if type(text) is str:
                                        if use_md:
                                            new_parts[-1] += text
                                        else:
                                            new_parts += [text]
                                        
                                    else:
                                        print(f"\nFunction {name} returned invalid value")
                                        new_parts[-1] += f"<i>Error: Function \"{name}\" not found</i>"
                                        
                                except Exception as e:
                                    print(f"\nFunction {name} had an error:")
                                    traceback.print_exc()
                                    new_parts[-1] += f"<i>Error: Function \"{name}\" not found</i>"
                                
                            else:
                                print(f"\nFunction {name} not found in file {info.site_path}functions/{name}.py")
                                new_parts[-1] += f"<i>Error: Function \"{name}\" not found</i>"
            
                        else:
                            print(f"\nFunction {name} not found")
                            new_parts[-1] += f"<i>Error: Function \"{name}\" not found</i>"
                            
                        converted = True
                        idx += 1
                        
            if not converted:
                if new_parts[-1][0] == ">":
                    new_parts[-1] += part
                else:
                    new_parts += [">" + part]
            even = not even
        html = ""
        for part in new_parts:
            if part[0] == ">":
                html += md.markdown(part[1:], output_format="html5", extensions=extensions)
            else:
                html += part
        
        info.file.content = bytes(html, "utf-8")
