import os
import htmlmin


class Module:
    def apply(self, info):
        minify = True
        if "min" in info.request_params and info.request_params["min"] == "false":
            minify = False
        
        if os.path.split(info.request_path)[0] == "/":
            nav = f'<li><a class="active" href=".">{info.site_config["name"]}</a></li>'
        else:
            nav = f'<li><a href="/">{info.site_config["name"]}</a></li>'
        for dir_ in os.listdir(info.site_path+"files/"):
            if dir_[0] != "." and os.path.isdir(info.site_path+"files/" + dir_):
                if info.request_path.split("/")[1] == dir_:
                    nav += "\n"+" "*16+f'<li><a class="active" href="/{dir_}/">{dir_}</a></li>'
                else:
                    nav += "\n"+" "*16+f'<li><a href="/{dir_}/">{dir_}</a></li>'
        
        ## create full html file
        values = {
            "content":     info.file.content,
            "title":       info.file.config["title"] or os.path.splitext(os.path.split(os.path.normpath(info.request_path))[1])[0].title(),
            "largeheader": " largeheader" if info.file.config["large-header"] else "",
            "path":        info.env["PATH_INFO"],
            "nav":         nav,
            "name":        info.site_config["name"],
        }
        html = info.modules["templates"]["module"].apply_template(values, "html", info)
        
        # get minimized css for this file
        css = info.modules["mini_css"]["module"].for_html(html, info, minify)
        
        # add css into html
        html = html.replace(b"{css}", css)
        
        # minimize html
        if minify:
            html = bytes(htmlmin.minify(str(html, "utf-8"), True, True, False, True, True, True), "utf-8")
        
        
        info.file.mimetype = ["text/html", None]
        info.file.content = html
