import os


class Module:
    def apply(self, info):
        if info.file.config["mode"] == "absolute":
            location = info.file.config["location"]
        else:
            location = os.path.join(info.env["PATH_INFO"], info.file.config["location"])
        
        info.response["headers"] += [("Location", location)]
        info.response["code"] = 301
        info.file.content = b""
