from PIL import Image
import io
import os
import warnings

warnings.simplefilter('error', Image.DecompressionBombWarning)


class Module:
    def apply(self, info):
        if "size" in info.request_params:
            if info.request_params["size"] == "small":
                self.shrink(info, 400, 200)
            elif info.request_params["size"] == "large":
                self.shrink(info, 1000, 1000)


    def shrink(self, info, width=None, height=None):
        path_parts = os.path.splitext(info.content_path)
        small_path = f"{info.content_path}-{width},{height}.webp"
        
        if os.path.exists(small_path):
            info.file.content = open(small_path, "rb").read()
            
        else:
            image = Image.open(io.BytesIO(info.file.content))
            out = io.BytesIO()
            
            if width < image.width or height < image.height:
                height_for_width = round(width/image.width * image.height)
                width_for_height = round(height/image.height * image.width)
                if height_for_width > width_for_height:
                    width = width_for_height
                else:
                    height = height_for_width
                
                image = image.resize((width, height))
                
            image.save(out, "WebP", lossless=False, quality=50, method=6)
            
            out.seek(0)
            info.file.content = out.read()
            open(small_path, "wb").write(info.file.content)
        
        info.file.mimetype = ["image/webp", None]
