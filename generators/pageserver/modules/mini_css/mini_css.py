import os
from cssmin import cssmin
from bs4 import BeautifulSoup
import tinycss


class Module:
    def apply(self, info):
        pass
    

    def for_html(self, html, info, minified=True):
        themes = info.site_config.get("theme", default="default").split(",")
        css = ""
        extra_css = ""
        for theme in themes:
            if os.path.exists(info.site_path+"/styles/"+theme+".css"):
                css_file = open(info.site_path+"/styles/"+theme+".css", "r").read()
            elif os.path.exists("generators/fileserver/themes/"+theme+".css"):
                css_file = open("generators/fileserver/themes/"+theme+".css", "r").read()
            else:
                print(f"\nStylesheet {theme} not found")
                css_file = "body:before {content:\"404 Stylesheet Not Found\";}"
            
            split = css_file.split("\n", 1)
            if split[0] == "#!shrink=false":
                extra_css += split[1]
            else:
                css += css_file
        
        soup = BeautifulSoup(html, "html.parser")
        
        sheet = tinycss.make_parser().parse_stylesheet(css)
        
        # remove any css not valid in html
        
        css_lines = css.split("\n")
        last_selector_line = 0
        was_required = False
        css = "\n"
        for rule in sheet.rules:
            if was_required:
                css += "\n".join(css_lines[last_selector_line-1:rule.selector.line-1])+"\n"
            
            last_selector_line = rule.selector.line
            
            selector = ",".join([part.split(":", 1)[0] for part in rule.selector.as_css().split(",")])
            if soup.select_one(selector):
                was_required = True
            else:
                was_required = False
        
        if was_required:
            css += "\n".join(css_lines[last_selector_line-1:])+"\n"
        
        css += extra_css
        
        if minified:
            return bytes(cssmin(css), "utf-8")
        else:
            return bytes(css, "utf-8")
