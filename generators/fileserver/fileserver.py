import tldextract
import os
import mimetypes
import time


class Generator:
    def __init__(self, path):
        self.directory = """
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>!name! - !path!</title>
        <link rel="stylesheet" type="text/css" href=".css">
    </head>
    <body>
        <header>
            <h1>Index of !path!</h1>
        </header>
        <main>
            <table>
                <tr>
                    <th>Name</th>
                    <th>Size</th>
                    <th>Modified</th>
                </tr>
                <tr>
                    <td><a href="../">../</a></td>
                    <td>-</td>
                    <td>-</td>
                </tr>!content!
            </table>
        </main>
        <footer>
            <br>
            <p>!footer!</p>
        </footer>
    </body>
</html>
"""

        self.error = """
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>!name! - 404</title>
        <link rel="stylesheet" type="text/css" href=".css">
    </head>
    <body>
        <header>
            <h1>404 - File !path! not found</h1>
        </header>
        <main>
            <a href="/">Return to /</a>
        </main>
        <footer>
            <br>
            <p>!footer!</p>
        </footer>
    </body>
</html>
"""

    def request(self, info):
        if os.path.split(info.request_path)[1] == ".css":
            theme = info.site_config["file_theme"]
            if not theme:
                theme = info.site_config.get("theme", default="default")
            if os.path.exists("generators/fileserver/themes/"+theme+".css"):
                info.response["content"] = open("generators/fileserver/themes/"+theme+".css", "rb").read()
                info.response["headers"] += [("Content-Type", "text/css")]
                info.response["code"] = 200
            else:
                print(f"\nStylesheet {theme} not found")
                info.response["content"] = b"body:before {content:\"404 Stylesheet Not Found\";}"
                info.response["headers"] += [("Content-Type", "text/css")]
                info.response["code"] = 404
            
        elif os.path.exists(info.content_path):
            if os.path.isdir(info.content_path):
                if info.env["PATH_INFO"][-1] == "/":
                    index = None
                    for item in os.listdir(info.content_path):
                        item_path = info.content_path + item
                        if not os.path.isdir(item_path):
                            if os.path.splitext(item)[0] == "index":
                                index = item_path
                                break
                    
                    if index is not None and info.site_config["use-index"] == "true":
                        
                        info.response["content"] = open(index, "rb").read()
                        
                        mime = mimetypes.guess_type(index)
                        info.response["headers"] += [("Content-Type", mime[0] if mime[0] is not None else "text/plain")]
                        if mime[1] is not None:
                            info.response["headers"] += [("Content-Encoding", mime[1])]
                            
                        info.response["code"] = 200
                    else:
                        content = ""
                        for item in os.listdir(info.content_path):
                            item_path = os.path.join(info.content_path, item)
                            if os.path.isdir(item_path):
                                item += "/"
                                size = "-"
                            else:
                                size = os.path.getsize(item_path)
                            content += f"""
                            <tr>
                                <td><a href="{item}">{item}</a></td>
                                <td>{size}</td>
                                <td>{time.strftime("%Y/%m/%d %H:%M:%S", time.localtime(os.path.getmtime(item_path)))}</td>
                            </tr>"""
                        info.response["content"] = bytes(self.directory.replace("!content!", content).replace("!name!", info.site_config["name"])\
                                    .replace("!footer!", info.site_config["footer"]).replace("!path!", info.request_path), "utf-8")
                        info.response["headers"] += [("Content-Type", "text/html")]
                        info.response["code"] = 200
                        
                else:
                    info.response["headers"] += [("Location", info.env["PATH_INFO"] + "/")]
                    info.response["code"] = 301
            else:
                info.response["content"] = open(info.content_path, "rb").read()
                mime = mimetypes.guess_type(info.content_path)
                info.response["headers"] += [("Content-Type", mime[0] if mime[0] is not None else "text/plain")]
                if mime[1] is not None:
                    info.response["headers"] += [("Content-Encoding", mime[1])]
                info.response["code"] = 200
        else:
            info.response["content"] = bytes(self.error
                        .replace("!name!", info.site_config["name"])
                        .replace("!footer!", info.site_config["footer"])
                        .replace("!path!", info.request_path), "utf-8")
            info.response["headers"] += [("Content-Type", "text/html")]
            info.response["code"] = 404
